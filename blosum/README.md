Vous pouvez téléchargez dans ce dossier les fichiers contenant les matrices de score BLOSUM, téléchargés par exemple sur le site NCBI.

Le contenu de ce dossier sera ignoré par git lors des commits vu qu'il est listé dans le fichier `.gitignore` à la racine du dépôt: en effet, il est inutile de placer une copie de ces fichiers sur votre dépôt vu que ce n'est pas votre travail, et que ces fichiers peuvent être téléchargés par quiconque à leur source originale.
