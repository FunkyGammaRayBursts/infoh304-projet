#include "prog_deffuc.h"

void tokenize(const string& str, const char* delim, vector<string>& out)
{
    char* token = strtok(const_cast<char*>(str.c_str()), delim);
    while (token != nullptr)
    {
        out.push_back(string(token));
        token = strtok(nullptr, delim);
    }
}
int getscore(const pair<char, char>& desiredKey, const  map<pair<char, char>, int>& matrix) {
    map<pair<char, char>, int>::const_iterator it = matrix.find(desiredKey);
    if (it != matrix.end()) {
        int score = it->second;
        return score;
    } else {
        return -13;
    }
}

/// algo smith pour retrouver les sequences
int smithWaterman(const string& query, const string& sequence, const  map<pair<char,char>,int>& matrix, const int gapOpenPenalty,const int gapExtentionPenalty) {
    int m = query.length();// c'est la sequence recherchée
    int n = sequence.length(); //sequence de comparaison de la base
    
    vector<vector<int>> score(m + 1,vector<int>(n + 1, 0));
    
    int maxScore = 0;
    
    for (int i = 1; i <= m; ++i) {
        for (int j = 1; j <= n; ++j) {
            int diagonalScore = score[i - 1][j - 1] + getscore(make_pair(query[i - 1], sequence[j - 1]),matrix);
            int leftScore = score[i][j - 1] - gapOpenPenalty;
            int topScore = score[i - 1][j] - gapExtentionPenalty;
            
            int currentScore = max(0, max(diagonalScore, max(leftScore, topScore)));
            score[i][j] = currentScore;
            
            if (currentScore > maxScore) {
                maxScore = currentScore;
            }
        }
    }
    
    return maxScore;
}

// premier int est le score et le second la position du seq dans la table.psq
vector<pair<int,int>> searchDatabase(const string& query, const vector<string>& database, const map<pair<char, char>, int>& matrix, const int gapOpenPenalty,const int gapExtentionPenalty) {
    vector<pair<int, int>> results;

    for (size_t i = 0; i < database.size(); ++i) {
        int score = smithWaterman(query, database[i], matrix,gapOpenPenalty,gapExtentionPenalty);
        results.push_back(make_pair(score, i));
    }

    // Tri des résultats par ordre décroissant du score
    sort(results.begin(), results.end(), greater<pair<int, int>>());

    // Tronquer les résultats pour ne conserver que les 20 meilleurs
    if (results.size() > 20) {
        results.resize(20);
    }

    return results;
}

int recherche_position(FILE* fp, const long int position){
   char c;
     long int found_position = 0; //Variable permettant de stoxker la position de 1A trouvée
     long int new_position = 0;
       fseek(fp, position, SEEK_SET);// ON se déplace à la position de départ
       
        while(!feof(fp))
        {			fread(&c,1,1, fp);// lecture d'un octet à la fois 
			
			if(c==(char)0x801A)// vérification si l'octet à la 1A
				{
					found_position = ftell(fp)-1; // stockage de la position ou la valeur 1A est trouvée
					break;					
			     }
		}
		fseek(fp, found_position+1, SEEK_SET);// ON se déplace à la position de départ
       
        while(!feof(fp))
        {
			fread(&c,1,1, fp);// lecture d'un octet à la fois 
			
			if(c==(char)0x801A)// vérification si l'octet à la 1A
				{
					new_position = ftell(fp)-1; // stockage de la position ou la valeur 1A est trouvée
					break;
					
			     }
		}
		return(new_position);
 }
    
string offset_lu(FILE* fp, int  position_found){	 
	int ofset_position = 0;
		ofset_position = position_found +1; // Position de l'octet de codant la taille de la chaine de caractère
		  fseek(fp,ofset_position, SEEK_SET); // positionnement du curseur au niveau de l'octet désiré dans le fichier
			 int byte; 
			   byte = fgetc(fp); // Lecture de l'octet
		
	int longueur_chaine = byte;
	 int i=0;
	  char hex[10]={0};
		string hex_chaine; 
	 
   while (i!=longueur_chaine)
      {
       ofset_position = position_found+i+2; // Position de l'octet codant la taille de la chaine de caractère
       fseek(fp,ofset_position, SEEK_SET); // positionnement du curseur au niveau de l'octet désiré dans le fichier
        int octet_lu; 
          octet_lu= fgetc(fp); // Lecture de l'octet
          if(octet_lu!=(char)0x20)
           {sprintf(hex, "%X", octet_lu);       
            printf("Les octets de la chaine lu en hexadécimal sont: %s\n", hex); 
              hex_chaine+=hex; 
             i++;}
           else
           break;            
      }
      return(hex_chaine);
}        
 char* ascii_chaine(string chaine){
	   int len = chaine.length();
       	cout<<len<<endl; 
		 char ascii_chaine[len/2]; // Initialisation de la chaîne ASCII

		  int j=0, valeur=0;
		   for (j = 0; j < len; j += 2) 
			{
			  sscanf(&chaine[j], "%2x", &valeur); // Conversion de chaque paire hexadécimale en entier
			  ascii_chaine[j/2] = (char)valeur; // Conversion de l'entier en caractère ASCII
			}
		   ascii_chaine[j/2] = '\0'; // On rajoute le caractère de fin de chaîne
		   cout<<ascii_chaine<<endl;
  return(ascii_chaine);
}

