#include <cstring>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <map>
#include <cstdint>
#include <cstddef>
#include <sstream>
using namespace std;
using std::string; using std::cout;using std::stringstream;
using std::ifstream;using std::vector;using std::endl;
using std::byte; using std::to_integer;
//******* lecture du fichier fasta.pin
int data_type(byte*bpin);
int title_length(byte*bpin);
string title(byte*bpin);
int timestamp_len(byte*bpin);
string timestamp(byte*bpin);
int nbre_seq(byte*bpin);
int nbre_residu(byte*bpin);
static int* list_ofseq(byte* bpin);
static int* list_ofta(byte* bpin);
int rech_header(int i, int*ofseq);
//**********************************************************************
static vector<string> tra_psq( const char *c, int nbre);// fichier .psq
string recup_seq( const vector<string>& contenu);//proteine de requete: query
vector<string> openf_prot( const string& nom_fichier);
void searchprot(const vector<string>& table, const string& prot);
//********************* matrice blosum
void tokenize(const string& str, const char* delim, vector<string>& out);
int getscore(const pair<char, char>& desiredKey, const  map<pair<char, char>, int>& matrix);
int smithWaterman(const string& query, const string& sequence, const  map<pair<char,char>,int>& matrix, const int gapPenalty);
int rechercher_proteine(const vector<string>& t, const string& proteine);
vector<pair<int,int>> searchDatabase(const string& query, const vector<string>& database, const map<pair<char, char>, int>& matrix, const int gapOpenPenalty,const int gapExtentionPenalty); 
//**************************************** fichier phr
int recherche_position(FILE* fp, const long int position);
string offset_lu(FILE* fp, int position_found);
 char* ascii_chaine(string chaine);


	

