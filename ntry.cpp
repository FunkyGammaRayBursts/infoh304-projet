#include "prog_deffuc.h"
int num_version(byte*bpin){
	static int version=0;
	if(version==0){
        version = to_integer<int32_t>(bpin[3]);
        return version;
        }
     else{
		 return version;
		 }
}

int data_type(byte*bpin){
	static int type=0;
	if(type==0){
        int t = to_integer<int32_t>(bpin[7]);
        type = t;
        return t;}
    else{
		return type;
		}
}

int title_length(byte*bpin){
        static int l=0;
        if(l==0){l = to_integer<int32_t>(bpin[11]);
        return l;}
        else{return l;}
}

string title(byte*bpin){
	static string word = "";
	if(word ==""){
		for (int m = 12; m < 40; m++) {
		string s = string(1, to_integer<int32_t>(bpin[m]));
		word += s;
		}
		return word;
	}
	else{
	return word;
	}
}

int timestamp_len(byte*bpin){
	static int len=0;
	if(len==0){ 
		len= to_integer<int32_t>(bpin[43]);
		}
	int l= len ;
	return l;
}

string timestamp(byte*bpin){
	static string word = "";
		if(word==""){
		for (int m = 44; m < 72; m++) 
		{
			string s = string(1, to_integer<int32_t>(bpin[m]));
			word += s;
		}
		return word;
	}
	else{
		return word;
		}
}

int nbre_seq(byte*bpin){
	static int32_t nbre=0;
	if(nbre==0){
		static int32_t nbre = to_integer<int32_t>(bpin[75]) << 0|
					   to_integer<int32_t>(bpin[74]) << 8 |
					   to_integer<int32_t>(bpin[73]) << 16|
					   to_integer<int32_t>(bpin[72]) << 24;
		return nbre;
		}
		else{
			return nbre;}
}

int nbre_residu(byte*bpin){
	static int64_t total=0;
	if(total==0){
		total = to_integer<int32_t>(bpin[76]) << 0|
						to_integer<int32_t>(bpin[77]) << 8 |
						to_integer<int32_t>(bpin[78]) << 16 |
						to_integer<int32_t>(bpin[79]) << 24 |
						to_integer<int32_t>(bpin[80]) << 32 |
						to_integer<int32_t>(bpin[81]) << 40 |
						to_integer<int32_t>(bpin[82]) << 48 |
						to_integer<int32_t>(bpin[83]) << 56 ;
		 
		  return total;
		  }
	  else{
		  return total;
		  }
}

static int* list_ofta(byte* bpin) {
    // Offsets into the sequence’s header file (*.phr, *.nhr).
    int n = 84;
    int nbre = 568744;
    static int* ofta = nullptr; // Utilisation d'un pointeur nul pour vérifier s'il a déjà été créé
    if (!ofta) {
        ofta = new int[nbre +2];
        for (int i = 0; i < nbre + 2; i++) {
            int32_t all = to_integer<int32_t>(bpin[n+3]) << 0 |
                      to_integer<int32_t>(bpin[n+2]) << 8 |
                      to_integer<int32_t>(bpin[n+1]) << 16 |
                      to_integer<int32_t>(bpin[n]) << 24;
            if (all == 0) {
                n += 4;
            } else {
                ofta[i] = all;
                n += 4;
            }
        }
         //cout << "l'indice : " << n << endl;
    }
    return ofta;
}

static int* list_ofseq(byte* bpin) {
    // Offsets into the sequence’s residue file (*.psq, *.nsq).
    int n = 2275080;
    int nbre = 568744;
    static int* ofseq = nullptr; // Utilisation d'un pointeur nul pour vérifier s'il a déjà été créé
    if (!ofseq) {
        ofseq = new int[nbre + 3];
        for (int i = 0; i < nbre + 3; i++) {
          int32_t all = to_integer<int32_t>(bpin[n+3]) << 0 |
                      to_integer<int32_t>(bpin[n+2]) << 8 |
                      to_integer<int32_t>(bpin[n+1]) << 16 |
                      to_integer<int32_t>(bpin[n]) << 24;
            if (all == 0) {
                n += 4;
            } else {
                ofseq[i] = all;
                n += 4;
            }
        }
    }
    return ofseq;
}


  


