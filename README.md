
     Rapport du projet de programmation en C++:

 Ce rapport permet d'expliquer le contenu de notre travail;
 Le but de ce travail est de rechercher la séquence de la protéine requette dans la base de donnée contenant les séquences de différentes protéines. 
 Pour ce faites, nous avons effectué un test de similarité entre la séquence de la protéine requette et ceux contenues dans la Database.
 Pour le faire, nous avons divisé le travail en trois parties:
 -Retrouver les informations de la protéine dans les différentes base de données
 -Ecrire l'algorithme de Smith-Waterman qui est un algorithme optimal qui nous donnera un alignement correspondant au meilleur score possible de correspondance 
  entre les séquences de la protéine requette et les séquences de la Database.
 -Effectuer une optimisation de algorithme pour la rendre plus optimal;
 
 1) Recherche des informations telle que: la position de la protéine dans la base psq et retrouver le header de la protéine query dans le fichier phr
     On a débuté par traduire le fichier psq en utilisant la fonction tra_psq. Ensuite on stocke la séquence de la protéine dans une string en utilisant la fonction openf_prot.
     Enfin on effectue une recherche de la séquence de la protéine dans le fichier psq traduit en utilisant la fonction rechercheProteine et on récupère l'indice i de la ligne 
     correspondant à sa position. On va rechercher par la suite l'offset du header de la protéine query dans le tableau des offset des headers contenu dans le fichier pin qui 
     nous donnera la position des headers des séquences codées dans le fichier phr de l'ensemble des séquences des protéines contenues dans le fichier psq. POur pouvoir traduire
     le fichier pin ,nous avons utilisé les fonctions  data_type,title_length, title,timestamp_len, timestamp, nbre_seq, nbre_residu, list_ofseq, list_ofta et rech_header. 
     Pour trouver la position de l'offset du header de la protéine query, nous avons utilisé la fonction rech_header. Ensuite pour avoir le header de la protéine query nous 
     avons dû naviguer dans le fichier phr. Pour trouver trouvé la position exacte du header du fichier phr, nous avons dû effectuer une recherche en utilisant la fonction 
     recherche_position à partir de la position qui nous a été donnée par la fonction rech_header effectuée sur le fichier pin du premier octet 1A car le header de notre protéine
      est codé dans le type VisibleString; En suite pour traduire les octet codé en type VisibleString on utilise les fonctions offset_lu et ascii_chaine (pour mettre sous forme 
      de chaine de caractère); Et la fonction va nous renvoyer une chaine de caractère caractérisant le header de la protéine query. Par la suite on a appliqué le même processus 
      au niveau des protéines prelim1,prelim2, prelim3 et prelim4.

 2)Pour la partie algorithmique, on a créé les fonctions SmithWaterman (qui est une implémentation de l'algorithme de Smith Waterman permettant de retrouver les séquences qui se rapprochaient 
 le plus de celle de query en comparant les séquences contenus dans la database avec celle de la protéine requette),
  searchDatabase ( qui crée un vecteur de pair en parcourant toute la Database et en effectuant une comparaison à chaque fois; Le premier élément de ce vecteur de pair est composé des 20 meilleurs 
  scores  selon l'algorithme de Smith Waterman comme premier élément et les deuxièmes éléments sont les indicies correspondant aux séquences des 20 meilleurs scores).
  pour avoir les scores et la position des 20 meilleurs protéines qui se rapproche le plus de la protéine requette selon l'algorithme de Smith Waterman). La fonction getscore permet de calculer le score
 de deux lettres de l'alphabet   Par la suite on utilise les fonctions ofta (pour avoir la position de chaque header correspondant des 20 meilleurs séquences) et les fonctions recherche_position, 
 offset_lu et ascii_chaine pour traduire les élément issue du fichier phr en lettre en utilisant le code Ascii pour ressortir les headers des différentes protéines correspondant aux 20 différentes séquences. 
  
  3) Pour optimiser le code, on a rendu les tableaux des offsets du phr et du psq contenus dans le fichier pin statique pour garder une copie entre les exécutions; On a effectué un passage par référence 
  dans toutes les fonctions pour éviter des copies inutiles lors de l'exécution de notre code;
  On a rajouté le mot clé const au début de certains variables comme mapblos, seq, et les databases par sécurité et pour éviter que ces variables soient modifiées;

