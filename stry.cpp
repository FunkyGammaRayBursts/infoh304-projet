
#include "prog_deffuc.h"
vector<string> openf_prot( const string & nom_fichier)
{
    vector<string> contenu;
    string ligne;
    ifstream f_prot(nom_fichier);
    if (!f_prot.is_open()) {
        cout << "Impossible d'ouvrir le fichier" << endl;
        return contenu;
    }

    while (getline(f_prot, ligne)){
        contenu.push_back(ligne);
    }
    f_prot.close();
    return contenu;
}

string recup_seq( const vector<string>& contenu)
{
	string word="";
	for (size_t i=1; i<size(contenu);i++)
		{
			word+=contenu[i];
		}
	return word;
}


static vector<string> tra_psq( const char *c, int nbre){
	 static vector<string>table;
	 string seq="";
	 if(table.empty()){
		for( int i=0;i< nbre;i++)
		{ 
			//vector<char>seq;
			char residu;
			if(c[i]==0)
			{
			table.push_back(seq);
			seq.clear();
			}
			if(c[i]==1)
			{ residu ='A';
			}
			if(c[i]==2)
			{ residu ='B';
			}
			if(c[i]==3)
			{ residu ='C';
			}
			if(c[i]==4)
			{ residu ='D';
			}
			if(c[i]==5)
			{ residu ='E';
			}
			if(c[i]==6)
			{ residu ='F';
			}
			if(c[i]==7)
			{ residu ='G';
			}
			if(c[i]==8)
			{ residu ='H';
			}
			if(c[i]==9)
			{ residu ='I';
			}
			if(c[i]==27)
			{ residu ='J';
			}
			if(c[i]==10)
			{ residu ='K';
			}
			if(c[i]==11)
			{ residu ='L';
			}
			if(c[i]==12)
			{ residu ='M';
			}
			if(c[i]==13)
			{ residu ='N';
			}
			if(c[i]==26)
			{ residu ='O';
			}
			if(c[i]==14)
			{ residu ='P';
			}
			if(c[i]==15)
			{ residu ='Q';
			}
			if(c[i]==16)
			{ residu ='R';
			}
			if(c[i]==17)
			{ residu ='S';
			}
			if(c[i]==18)
			{ residu ='T';
			}
			if(c[i]==24)
			{ residu ='U';
			}
			if(c[i]==19)
			{ residu ='V';
			}
			if(c[i]==20)
			{ residu ='W';
			}
			if(c[i]==21)
			{ residu ='X';
			}
			if(c[i]==22)
			{ residu ='Y';
			}
			if(c[i]==23)
			{ residu ='Z';
			}
			if(c[i]==25)
			{ residu ='*';
			}
			seq+=residu;	
	}
}
	return table;
}


int rechercher_proteine(const vector<string>& t, const string& proteine) {
    for (size_t i = 0; i < t.size(); ) {
        if (t[i].rfind(proteine) != string::npos) {
            return i;
        } else {
            i++;
        }
    }
    return -1;  // Si la protéine n'est pas trouvée
}
				

