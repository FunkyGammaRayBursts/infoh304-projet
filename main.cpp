#include "prog_deffuc.h"
#include "ntry.cpp"
#include "stry.cpp"
#include "smith.cpp"

int main(int argc, char* argv[])
{
	if (argc != 6) {
		// verifie qu'il y a bien 6 arguments
        cout << "Nombre incorrect d'arguments." <<endl;
        cout << "Utilisation: ./projet query/P00533.fasta database/uniprot_sprot.fasta blosum/BLOSUM62 11 1" << endl;
        return 1;
    }

    string queryFile = argv[1];//file de la proteine requete
    string databaseFile = argv[2];// file de la base de donnée
    string blosumFile = argv[3];// file de la matrice blosum62
    int gapOpenPenality = stoi(argv[4]);// les gap de penalité
    int gapExtensinPenality= stoi(argv[5]);
    ifstream file(blosumFile);
    vector<vector<int>> matrix;
    vector<char> letters;
    map<pair<char, char>, int> mapblos;
    
    if (file.is_open()) {
        string line;
        int lskip = 7;
        
        for (int i = 0; i < lskip; i++) {
            getline(file, line);
        }
        
        const char* delim = " ";
        vector<string> out;
        tokenize(line, delim, out);
        
        for (const string& s : out) {
            letters.push_back(s[0]);
        }
        
        while (getline(file, line)) {
            istringstream iss(line);
            string s;
            vector<int> numbers;
            
            while (getline(iss, s, ' ')) {
                try {
                    int num = stoi(s);
                    numbers.push_back(num);
                }
                catch (invalid_argument&) {
                }
            }
            
            matrix.push_back(numbers);
        }
        
        file.close();
    }
    else {
        cout << "Le fichier n'est pas ouvert" << endl;
        return 0;
    }
    
    for (size_t i = 0; i < matrix.size(); i++) {
        for (size_t j = 0; j < matrix.size(); j++) {
            mapblos[make_pair(letters[i], letters[j])] = matrix[i][j];
        }
    }
    
    //********************** fichier .pin
        // Ouvrir le fichier .pin
    FILE* fpin;
    string pinFile = databaseFile + ".pin";
    fpin = fopen(pinFile.c_str(), "rb");
    vector<string> all = openf_prot(queryFile);
    FILE* fphr;
    string phrFile = databaseFile + ".phr";
	fphr = fopen(phrFile.c_str(), "rb");
	FILE* fpsq ;
	string psqFile = databaseFile + ".psq";
	fpsq = fopen(psqFile.c_str(), "rb");
	if (!fpsq) 
	{
		printf("impossible d'ouvrir le fichier");
		return -1;
	}

    if (!fpin) {
        
       cout << "Impossible d'ouvrir le fichier";
       return 1;
        } 
    else {
		
        long lSize2;
        byte* bpin;
        fseek(fpin, 0, SEEK_END);
        lSize2 = ftell(fpin);
        rewind(fpin);
        bpin = (byte*)malloc(lSize2);
        fread(bpin, sizeof(byte), lSize2, fpin);
        int *ofta=list_ofta(bpin);// offset phr

		//*************************************************************
		// fichier psq et query
		 long lSize;
		 char * base;
		 fseek(fpsq, 0,SEEK_END);
		 lSize=ftell(fpsq);
		 rewind(fpsq);
		 base = (char*)malloc(lSize);
		 fread(base,sizeof(char),lSize,fpsq);
		 vector<string>table=tra_psq(base,lSize);
		 string seq=recup_seq(all);
		 // le premier int est le score et le second int la position des sequences
		 vector<pair<int,int>>position = searchDatabase(seq, table, mapblos, gapExtensinPenality,gapOpenPenality); 
		 for (const auto& pair : position) {
			cout << "Premier élément : " << pair.first << ", Deuxième élément : " << pair.second << endl;
			}
		vector<int> headers;
		for (const auto& result : position) {
			headers.push_back(result.second);
		}
		for (size_t i=0;i<headers.size();i++){
			long int k =headers[i];// recupere l'indice de la proteiene 
			long int = ofta[k];// donne la position du debut du header de la proteine
		}
		
		
		 free(base);
		 free(bpin);
		 delete[]ofta;
		 fclose(fpin);
		 fclose(fpsq);
		 fclose(fphr);
		}
	 //*****************************************************************
    
    return 0;
}
