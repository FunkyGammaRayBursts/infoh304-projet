CC = g++
CFLAGS = -std=c++11 -Wall
SOURCES = main.cpp stry.cpp ntry.cpp smith.cpp
HEADERS = prog_deffuc.h
EXECUTABLE = projet

all: $(EXECUTABLE)

$(EXECUTABLE): $(SOURCES) $(HEADERS)
    $(CC) $(CFLAGS) $(SOURCES) -o $(EXECUTABLE)

clean:
    rm -f $(EXECUTABLE)

